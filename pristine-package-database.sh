#!/bin/bash

cd $(dirname $0)

mkdir -p pkgdb

rm -f pkgdb/*

for file in $(dpkg -L ghc|grep package.conf.d)
do
	if test -d $file; then continue; fi
	ln -s $file pkgdb/
done

ghc-pkg recache --package-db=pkgdb
